alphabet = "abcdefghijklmnopqrstuvwxyz"

def strip_duplicates(input):
    seen = set()
    seen_add = seen.add
    return [x for x in input if not (x in seen or seen_add(x))]

def encode_keyword(string, keyword):
    string = string.lower()
    keyword = keyword.replace(" ", "")
    substitution_alphabet = ""
    new_string = ""
    #makes a set of unique chars
    stripped_keyword = strip_duplicates(keyword)
    #turns the set into  a string
    substitution_alphabet = stripped_keyword
    #takes each char in the alphabet and adds it if it isn't already in the stripped word
    for character in alphabet:
        if character not in stripped_keyword:
            substitution_alphabet += character
    for letter in string:
        if letter in alphabet:
            position = alphabet.find(letter)
            new_string += substitution_alphabet[position]
        else:
            new_string += letter
    return new_string
